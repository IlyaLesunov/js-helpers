console.log('Подключен')

/*
Как пользоватся:

1) Создаем новый класс

2) Передаем значения формы

3) form: '.form-request', <- Передаем класс(.example) или id(example) формы
    tagName: 'name', <- имя тега в нем вписываеются названия поля. ( в input -> tagName: name; input -> tagName: email)
    disabledBtn: true <- отключение кнопки
    validateInputs: {  <- Если нужна валидация
        name: {  <- Имя поля которая внесена в tagName и которые есть в форме(они должны совпадать)
            minLength: 0, <- Минимальное значение длины input
            text: 'Поле не должно быть пустым' <- Текст который будет оттображаться в поле ошибки
        },
        password: {
            minLength: 3,
            maxLength: 5, <- Максимальное значение длины input
            text: 'Поле должно содержать от 3 до 5 символов'
        },
        email: {
            type: 'email', <- Тип input (пока есть сверка на email)
            text: 'Введите корректный e-mail'
        }
    },
    fieldsForm: { <- Поля вашей формы куда записываются данные
        name: '',
        email: '',
        password: ''
    }

    4) При отправке данных данные можно получить через метод getListForm
    5) Послше успешной отправки данных стоит все подчистить с помощью метода clearAllFields

    Методы:

    1) observeErrorLayout - Возвращает true или false. Служит для защиты. Во время нажатие на кнопку он может вернуть bool значение и если вернет false, то форма не отправится.
*/

class getDataFromInputs {

    constructor(payload) {

        const { form, tagName, fieldsForm, validateInputs, disabledBtn } = payload

        this.form = form || ''
        this.tagName = tagName || ''
        this.fieldsForm = fieldsForm || {}
        this.validateInputs = validateInputs || {}
        this.button = disabledBtn
        this.setInput = []
        this.errors = []
        this.selectTag = ''
        this.inputText = ''
        this.selectinput = ''
        this.selectBtn = null
        this.count = 0
        this.getInputs()
    }

    // Получаем все input и textarea относящиеся к форме которую мы передаем в классе. Записываем все общий массив setInput для переиспользования глобально

    getInputs() {
        const getInputs = document.querySelectorAll(`${this.form} > input`)
        const getTextarea = document.querySelectorAll(`${this.form} > textarea`)
        if (this.button) {
            this.selectBtn = document.querySelector(`${this.form} > button`)
        }
        this.setInput = [...this.setInput, ...getTextarea, ...getInputs]
        this.createFieldError()
        this.listenerInputs()
    }

    // Создаем новый массив для записи ошибок относящиеся к полям

    createFieldError () {
        for (const key in this.fieldsForm) {
            this.errors = [...this.errors, { name: key, error: '' }]
        }
    }

    // Слушаем все input относящиеся к форме и чистим

    listenerInputs() {
        this.setInput.forEach((input) => {
            input.addEventListener('input', (e) => this.setInputsForms(e))
            input.removeEventListener('input', (e) => this.setInputsForms(e))
        })
    }

    // Записываем в форму все получаемые данные c input

    setInputsForms (e) {
        for (const key in this.fieldsForm) {
            const getAttribute = e.target.getAttribute(`${this.tagName}`)
            // Если attribute input совпадает с ключем передаваемой формы, то мы записываем данные в форму
            if (getAttribute === key) {
                // Записываем глобально данные активного input
                this.selectTag = getAttribute
                this.selectinput = e.target
                this.inputText = e.target.value
                // Если тип input равен checkbox, то мы запускаем другую запись
                if (e.target.type === 'checkbox') {
                    // Если тип поля формы равен boolean , то мы перезаписываем данные этой формы на true или false
                    if (typeof this.fieldsForm[this.selectTag] === 'boolean') {
                        this.fieldsForm[this.selectTag] = this.fieldsForm[key] ? false : true
                        this.checkedInputRequired()
                    } else {
                        // Если в форме поля массива присуствует значения поступающее с input, то мы удаляем это значение
                        if (this.fieldsForm[this.selectTag].includes(this.inputText)) {
                            this.fieldsForm[this.selectTag] = this.fieldsForm[this.selectTag].filter( function(payload) { return payload != e.target.value } )
                            // Иначе записываем в массив
                            this.checkedInputRequired()
                        } else {
                            this.fieldsForm[this.selectTag] = [...this.fieldsForm[this.selectTag], this.inputText]
                            this.checkedInputRequired()

                        }
                    }
                    //    Если данные input поступает строкой, то мы записываем в обычном виде
                } else {
                    this.fieldsForm[this.selectTag] = this.inputText
                    //    Запускаем валидацию на длину input
                    this.validateLengthInput()
                    //    Запускаем валидацию на тип input
                    this.validateTypeInput()
                    this.checkedInputRequired()
                }
            }
        }
    }

    // Проверка input на заданную длину

    validateLengthInput() {
        const { minLength, maxLength, text, classTextError } = this.validateInputs[this.selectTag]
        if (this.inputText?.trim()?.length <= minLength) {
            this.mainAddErrorLayout({text, classTextError})
        } else if (this.inputText?.trim()?.length > maxLength) {
            // Если валидацию не проходим, то записываем в массив ошибок ошибку относяшиеся к этому ключу поля, передав значение текста
            this.mainAddErrorLayout({text, classTextError})
        } else {
            // Если проходим проверку, то удаляем с DOM дерева созданную ошибку
            this.mainRemoveErrorLayout(classTextError)
        }
    }

    // Проверка input на тип поля

    validateTypeInput() {
        const { type, text, classTextError } = this.validateInputs[this.selectTag]
        if (type === 'email') {
            if (!this.validEmail(this.form.email)) {
                this.mainAddErrorLayout({text, classTextError})
            } else {
                this.mainRemoveErrorLayout(classTextError)
            }
        }
    }


    checkedInputRequired() {
        const { required, text, classTextError } = this.validateInputs[this.selectTag]
        if (required) {
            if (typeof this.fieldsForm[this.selectTag] === 'boolean') {
                if (!this.fieldsForm[this.selectTag]) {
                    this.mainAddErrorLayout({text, classTextError})
                } else {
                    this.mainRemoveErrorLayout(classTextError)
                }
            } else if (Array.isArray(this.fieldsForm[this.selectTag]) && typeof this.fieldsForm[this.selectTag] === 'object') {
                if (this.fieldsForm[this.selectTag].length < 1) {
                    this.mainAddErrorLayout({text, classTextError})
                } else if (this.fieldsForm[this.selectTag].length >= 1) {
                    this.mainRemoveErrorLayout(classTextError)
                }
            }
        } else {
            return
        }
    }

    // Проверка input на email

    validEmail () {
        let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(this.inputText);
    }


    // Общий класс для добавления ошибок

    mainAddErrorLayout (payload) {
        const { text, classTextError } = payload
        console.log(payload)
        // Если валидацию не проходим, то записываем в массив ошибок ошибку относяшиеся к этому ключу поля
        this.addErrorField(text)
        // Выводим ошибку под input
        this.addErrorLayout(classTextError)
        this.button ? this.observeErrorLayout() : ''
    }

    // Общий классс для удаление ошибки

    mainRemoveErrorLayout (classTextError) {
        this.removeErrorLayout(classTextError)
        this.button ? this.observeErrorLayout() : ''
    }

    // Удаляем класс с error

    removeErrorLayout(errorClass) {
        const attribute = this.selectinput.getAttribute('type')
        if (attribute === 'checkbox') {
            if (this.selectinput?.nextElementSibling?.nextSibling?.classList?.contains(`${errorClass}`)) {
                this.selectinput?.nextElementSibling?.nextSibling?.remove()
                this.count = 0
            }
        }
        if (this.selectinput.nextSibling?.classList?.contains(`${errorClass}`)) {
            this.selectinput.nextSibling.remove()
            this.errors.forEach((field) => {
                if (field.name === this.selectTag) {
                    field.error = ''
                }
            })
        }
    }

    // Добавляем ошибку в массив ошибок

    addErrorField(text) {
        this.errors.forEach((field) => {
            if (field.name === this.selectTag) {
                field.error = text
            }
        })
    }

    // Выводим ошибку под input

    addErrorLayout (classError) {
        console.log(classError)
        const attribute = this.selectinput.getAttribute(`${this.tagName}`)
        if (this.selectinput?.nextSibling?.hasChildNodes()) {
            return

        } else if (this.selectinput?.nextElementSibling?.nextSibling?.hasChildNodes()) {
            return
        }
        else {
            this.errors.forEach((input) => {
                const { name, error } = input
                if (attribute === name) {
                    if (attribute === 'checked') {
                        const checkBoxLabel = this.selectinput.nextElementSibling
                        checkBoxLabel.insertAdjacentHTML('afterend',
                            `<p  class="inputError ${classError}">${error}</p>`);
                    } else if (this.selectinput.type === 'checkbox') {
                        if (Array.isArray(this.fieldsForm[this.selectTag]) && typeof this.fieldsForm[this.selectTag] === 'object') {
                            const checkBoxLabel = this.selectinput.nextElementSibling
                            console.log('Да')
                            if (this.count === 0) {
                                this.count = this.count + 1
                                checkBoxLabel.insertAdjacentHTML('afterend',
                                    `<p  class="inputError ${classError}">${error}</p>`);
                            } else {
                                this.mainRemoveErrorLayout(classError)
                            }

                        } else {
                            const checkBoxLabel = this.selectinput.nextElementSibling
                            checkBoxLabel.insertAdjacentHTML('afterend',
                                `<p  class="inputError ${classError}">${error}</p>`);
                        }

                    } else {
                        this.selectinput.insertAdjacentHTML('afterend',
                            `<p class="inputError ${classError}">${error}</p>`);
                    }

                }
            })
        }


    }

    // TODO слушатель на ошибку

    observeErrorLayout () {
        const checkError = document.querySelectorAll('.inputError')
        if (checkError.length > 0) {
            this.selectBtn.setAttribute('disabled', 'disabled')
            return false
        } else {
            this.selectBtn.removeAttribute('disabled')
            return true
        }
    }


    // Подчищаем все поля и удаляем слушателя

    // clearAllFields () {
    //     this.form = ''
    //     this.tagName = ''
    //     for(const key in this.fieldsForm) {
    //         this.fieldsForm[key] = ''
    //     }
    //     this.setInput.forEach((input) => {
    //             input.removeEventListener('input', (e) => this.setInputsForms(e))
    //         if (input.value !== '') {
    //             input.value = ''
    //             this.setInput = []
    //         }
    //     })
    // }

    // Получаем данные с заполненной формы

    get getListForm () {
        return this.fieldsForm
    }
}

const data = {
    form: '.form-req',
    tagName: 'name',
    disabledBtn: true,
    validateInputs: {
        mail: {
            type: 'email',
            classTextError: 'text-error',
            text: 'Введите корректный e-mail'
        },
        name: {
            minLength: 0,
            classTextError: 'text-error',
            text: 'name не должен быть пустым'
        },
        password: {
            minLength: 6,
            maxLength: 16,
            classTextError: 'text-error',
            text: 'Пароль должен быть от 6 до 16 символов'
        },
        description: {
            minLength: 10,
            maxLength: 120,
            classTextError: 'text-error',
            text: 'description должен быть от 10 до 120 символов'
        },
        select: {
            required: true,
            classTextError: 'check-error',
            text: 'description должен быть от 10 до 120 символов'
        },
        checkbox: {
            required: true,
            classTextError: 'check-error',
            text: 'checkbox'
        }
    },
    fieldsForm: {
        name: '',
        mail: '',
        password: '',
        description: '',
        select: [],
        checkbox: false
    }
}


const newForm = new getDataFromInputs (data)

const btn = document.querySelector('.btn-send')

btn.addEventListener('click', () => {
    const check = newForm.observeErrorLayout()
    if (check) {
        console.log('newForm.getList', newForm.getListForm)
    } else {
        console.log('Нет')
    }
    // console.log('newForm.getList', newForm.getListForm)
})
