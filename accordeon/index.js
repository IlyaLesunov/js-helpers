console.warn('Accordeon Подключен')


/* TODO переписать на более оптимизированный вариант */

let allButton, activeAllClass, allTab, typeTab;


const toogleTitleTab = document.querySelector('.toogle-title-tab')
const btnToggleTab = document.querySelector('.btn-toggle-type-tab')

btnToggleTab.addEventListener('click', () => {
    if (typeTab) {
        toogleTitleTab.innerText = 'Обычные табы'
        btnToggleTab.innerText = 'Переключить на cамозакрывающиеся табы'
        typeTab = false

    } else {
        toogleTitleTab.innerText = 'Самозакрывающиеся табы'
        btnToggleTab.innerText = 'Переключить на обычные табы'
        typeTab = true
    }
})




const startAccordeon = (mainClassButton='',
                        mainClassTab='',
                        activeClassButton='',
                        activeClassTab='',
                        deactiveClass='',
                        autoAccordeon = false
) => {

    /* Выдергиваем все классы кнопок */

    const allClassButton = document.querySelectorAll(`.${mainClassButton}`)

    /* Выдергиваем все классы табов */

    const allClassTab = document.querySelectorAll(`.${mainClassTab}`)

    /* Выдергиваем все параметры в глобальные переменные */

    allButton = allClassButton
    activeAllClass = { activeClassButton, activeClassTab, deactiveClass, autoAccordeon }
    allTab = allClassTab
    typeTab = autoAccordeon
    /* Пробегаемся по всем кнопкам в макете */

    allClassButton.forEach(item => {
        listenerButton({item: item})
    })
}

const listenerButton = (itemsButton) => {
    const { item } = itemsButton
    item.addEventListener('click', function (event) {
        findClassButton({event: event})
    })
}

/* Находим определенный класс в кнопках + если найден активный класс удаляем, если нет - добавляем  */

const findClassButton =  (eventObj) => {
    const {event, activeClass} = eventObj
    const { activeClassButton, autoAccordeon } = activeAllClass
    event.target.classList.forEach(async (item) => {

        /* Если параметр стоит autoAccordeon true запускаем поиск всех кнопок и табов для добавления и удаления */

        if (typeTab) {
            findClassAllActive(event)
            return
        }

        /* Если параметр стоит autoAccordeon false, то мы либо удаляем класс с этой кнопки или добавляем */

        if (item === activeClassButton) {
            event.target.classList.remove(`${activeClassButton}`)
        } else {
            event.target.classList.add(`${activeClassButton}`)
            findClassActiveTab({event: event, activeClass})
        }
    })
}

/* Если параметр стоит autoAccordeon false, то мы либо удаляем класс с этого таба или добавляем + добавляем класс с анимацией закрытия */

const findClassActiveTab = (eventButton) => {
    const { event } = eventButton
    const  { deactiveClass, activeClassTab } = activeAllClass
    event.target.nextElementSibling.classList.forEach((tab) => {
        if (tab === activeClassTab) {
            event.target.nextElementSibling.classList.remove(activeClassTab)
            event.target.nextElementSibling.classList.add(deactiveClass)
        } else {
            event.target.nextElementSibling.classList.add(activeClassTab)
        }
    })
}

/* Если параметр стоит autoAccordeon true, пробигаемся по массиву всех кнопок и всех табов */

const findClassAllActive = (eventClass) => {
    const { activeClassButton, activeClassTab, deactiveClass } = activeAllClass
    allButton.forEach(item => {
        item.classList.forEach( async( classButton) => {
            /* Если параметр стоит autoAccordeon true и найден активный класс у кнопки удаляем этот класс */
            if (classButton === activeClassButton) {
                await item.classList.remove(activeClassButton)
            }
        })
    })

    allTab.forEach(item => {
        item.classList.forEach( async( classTabs) => {
            /* Если параметр стоит autoAccordeon true и найден активный класс у таба удаляем этот класс + добавляем класс анимации для закрытия */
            if (classTabs === activeClassTab) {
                await item.classList.remove(activeClassTab)
                await item.classList.add(deactiveClass)
            }
        })
    })

    addAllClass(eventClass)
}

/* Добавляем класс на нажатый эелемент + на соседний элемент таба */

const addAllClass = (eventClass) => {

    const { activeClassButton, activeClassTab, deactiveClass } = activeAllClass

    eventClass.target.classList.add(activeClassButton)
    eventClass.target.nextElementSibling.classList.add(activeClassTab)
    eventClass.target.nextElementSibling.classList.add(deactiveClass)
}


/* Подставялем сюда свои классы.
* mainClassButton - классы на которые будет нажимать пользователь для вывода таба
* mainClassTab - классы в которых содержиться информация таба
* activeClassButton - класс для выделения кнопки
* activeClassTab - класс для активного таба
* deactiveClass - класс для обратной анимации
* autoAccordeon - boolean параметр для включение другого типа accordeon
* false -  аккордеон который не закрывается соседние вкладки.
* true - аккордеон который закрывает соседние вкладки */

startAccordeon(
    'btn-tab',
    'tab-main',
    'btn-active',
    'tab-active',
    'tab-deactive'
)

