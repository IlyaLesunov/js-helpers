console.log('Подключен')

const startHeight = (mainLayoutClass="", massiveLayoutClass=[]) => {
    if (massiveLayoutClass.length === 0) {
        return console.log('error: [massiveLayoutClass] : не должен быть пустым')
    }
    let massiveHeight = []
    massiveLayoutClass.forEach((index) => {
        let classItem = document.querySelectorAll(`.${index}`)
        if ( classItem.length === 0) {
            return console.log('error: [massiveLayoutClass] : Такого класса нет')
        } else {
            let heightItem = classItem[0].clientHeight
            massiveHeight.push(heightItem)
        }

    })
    calculationHeight({massiveHeight: massiveHeight, mainLayoutClass: mainLayoutClass})
}

const calculationHeight = (itemHeight) => {
    const { massiveHeight, mainLayoutClass } = itemHeight
    if (massiveHeight.length === 0) {
        return console.log('error: [massiveLayoutClass] : Такого класса нет')
    } else {
        const finalSum = massiveHeight.reduce( function(previousNumber, currentNumber) {
            return previousNumber + currentNumber;
        });
        adaptiveHeight({finalSum: finalSum, mainLayoutClass: mainLayoutClass })
    }
}

const adaptiveHeight = (itemSumm) => {
    const { finalSum, mainLayoutClass } = itemSumm
    let classMain = document.querySelector(`.${mainLayoutClass}`)

    window.addEventListener("load", () => {
        const loadHeight = document.documentElement.clientHeight - finalSum + "px"
        classMain.style.height = loadHeight
        classMain.style.overflowY = 'scroll'
    });

    window.addEventListener("resize", () => {
        const resizeHeight = document.documentElement.clientHeight - finalSum + "px"
        classMain.style.height = resizeHeight
        classMain.style.overflowY = 'scroll'
    });

}



startHeight('main', ['top', 'bottom', 'main-test'])
