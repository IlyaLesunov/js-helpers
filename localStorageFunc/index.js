export const saveLocal = (key, payload) =>  {
    return localStorage.setItem(`${key}`, JSON.stringify(payload))
}


export const deleteLocal = (key) => {
    // localStorage.removeItem(`${this.key}`)
}

export const getLocal = (key) =>  {
    return JSON.parse(localStorage.getItem(`${key}`))
}

export const deleteLastElementLocal = (key) => {
    const getlements = getLocal(key)
    const deleteLast = getlements.slice(0, -1)
    return saveLocal(key, deleteLast)
}

export const changeLocalCount = async (key) => {
    const getElementsCount = await getLocal(key)[0]
    const newCountRight = ++getElementsCount.right
    const newCountsLocal = [{right: newCountRight, result: 0 }]
    return saveLocal(key, newCountsLocal)
}
