

// Иницилизируем наше приложение
(function (){

//    Добавим bootstrap css в head
    const bootstrapCss = document.createElement('link');
    bootstrapCss.crossorigin="anonymous"
    bootstrapCss.rel="stylesheet"
    bootstrapCss.href = 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'
    bootstrapCss.integrity="sha383-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"

    document.head.append(bootstrapCss);

//    Добавим разметку
    const getFetchID = document.getElementById("fetch");
    const mainSteelSheet =
        `<div class="container mt-5" id="main-wrapper">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-default">URL</span>
                    </div>
                    <input id="input" value="" type="text" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-12">
                <button onClick="getFetchData" type="submit" id="btnSend" class="btn btn-primary col-sm-12">Получить данные</button>
            </div>
        </div>
    </div>`


    getFetchID.insertAdjacentHTML('afterbegin', mainSteelSheet)

})();

const mainWrapper = document.getElementById('main-wrapper')
const btnSend = document.getElementById('btnSend')

// Получаем данные с сервера
const getFetchData = (inputTest) => {
    const input = document.getElementById("input").value
    const checkInput =  inputValidate({input, search: /https:/g})
    if (inputTest) {
        fetch(inputTest)
            .then(response => response.json())
            .then(data => {
                if (Object.keys(data).length < 1){
                    setErrorInHtml('Обьект пришел пустой')
                } else {
                    // Создаем дополнительные кнопки с key/value
                    setKeyValueButton(data)
                    // Создаем разметку карточки + данные
                    setSuccessHTML(data)
                }
            })
            .catch((error) => setErrorInHtml('Такой запрос не найден'))
    } else {
        console.log('Нет')
        setErrorInHtml('Ссылка должна начинаться на https://')
    }
}

btnSend.addEventListener('click', () => getFetchData('https://jsonplaceholder.typicode.com/todos/1'))
btnSend.removeEventListener('click', () => getFetchData('https://jsonplaceholder.typicode.com/todos/1'))

// Проверим какие данные приходят c input

const inputValidate =  (value) => {
    const {input, search} = value
    const checkURL = input.match(search)
    if (checkURL !== null) {
        return true
    } else {
        return false
    }
}


// При неправильной запросе будет выдавать ошибку.

const setErrorInHtml = (error) => {
    const divError = document.createElement('div');
    divError.classList = 'alert alert-danger'
    divError.id = 'alert'
    divError.innerText = JSON.stringify(error)
    mainWrapper.appendChild(divError)
    setTimeout(() => {
        const alertClass = document.getElementById('alert')
        mainWrapper.removeChild(alertClass)
    }, 3000)
}
// Создаем дополнительные кнопки с key/value
const setKeyValueButton = (data) => {
    const dataTest = {"userId":1,"id":1,"title":"delectus aut autem","completed":false, data: { check: false }}
    // Проверим приходящие данные на тип
    console.log('type', Array.isArray(dataTest))
    if (Array.isArray(dataTest)) {
        console.log('massive')
    } else {
        // Выдергиваем key/value
        getKeyValueObject(dataTest)
    }
}


// Выдергиваем key/value
const getKeyValueObject = (dataTest) => {

}


// Если ответ пришел пушим в дерево верстку с ответом

// Счетчик id для div c картачкой
let numberID = 0

const setSuccessHTML = (data) => {
    numberID++
    const divSuccess = document.createElement('div');
    const preData = document.createElement('code');
    const buttonDelete = document.createElement('button');
    preData.classList = 'alert-link'
    preData.innerText = JSON.stringify(data)
    divSuccess.classList = `request request-${numberID} alert alert-dark mt-5 mb-5`
    divSuccess.role = 'alert'
    buttonDelete.id = `request-${numberID}`
    buttonDelete.classList = 'btn btn-sm btn-danger mb-4 col-12'
    buttonDelete.textContent = 'Удалить'
    divSuccess.appendChild(buttonDelete)
    divSuccess.appendChild(preData)
    mainWrapper.appendChild(divSuccess);
}


// Удалим div с запросом
const deleteRequestElement = (e) => {
    if (e.target.id) {
        const getRequestDiv = document.querySelectorAll(`.request`)
        getRequestDiv.forEach((item) => {
            console.log('numberID', numberID)
            if (item.classList[1] === e.target.id) {
                mainWrapper.removeChild(item)
                numberID + 1
            }
        })
    } else {
        return
    }
}


document.body.addEventListener('click', (e) => deleteRequestElement(e))
document.body.removeEventListener('click', (e) => deleteRequestElement(e))






