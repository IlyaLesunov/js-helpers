console.log('Подключен')

class getDataFromInputs {

    constructor(payload) {

        const { form, tagName, fieldsForm } = payload

        this.form = form || ''
        this.tagName = tagName || ''
        this.fieldsForm = fieldsForm || {}
        this.setInput = []
        this.getInputs()

    }

    getInputs() {
        const getInputs = document.querySelectorAll(`${this.form} > input`)
        const getTextarea = document.querySelectorAll(`${this.form} > textarea`)
        this.setInput = [...this.setInput, ...getTextarea, ...getInputs]
        this.listenerInputs()
    }

    listenerInputs() {
        this.setInput.forEach((input) => {
            input.addEventListener('input', (e) => this.setInputsForms(e))
            input.removeEventListener('input', (e) => this.setInputsForms(e))
        })
    }

    setInputsForms (e) {
        for (const key in this.fieldsForm) {
            if (e.target.getAttribute(`${this.tagName}`) === key) {
                const data = e.target.getAttribute(`${this.tagName}`)
                if (e.target.type === 'checkbox') {
                    if (typeof this.fieldsForm[data] === 'boolean') {
                        this.fieldsForm[data] = this.fieldsForm[key] ? false : true
                    } else {
                        if (this.fieldsForm[data].includes(e.target.value)) {
                            this.fieldsForm[data] = this.fieldsForm[data].filter( function(payload) { return payload !== e.target.value} )
                        } else {
                            this.fieldsForm[data] = [...this.fieldsForm[data], e.target.value]
                        }
                    }
                } else {
                    this.fieldsForm[data] = e.target.value
                }
            }
        }
    }

    destroy() {
        this.setInput.forEach((input) => {
            input.removeEventListener('input', (e) => this.setInputsForms(e))
        })
    }

    clearAllFields () {
        this.form = ''
        this.tagName = ''
        for(const key in this.fieldsForm) {
            this.fieldsForm[key] = ''
        }
        this.setInput.forEach((input) => {
            if (input.value !== '') {
                input.value = ''
                this.setInput = []
            }
        })
        console.log('this.fieldsForm.length', this.fieldsForm)
    }

    get getListForm () {
        return this.fieldsForm
    }
}

const data = {
    form: '.form-request',
    tagName: 'name',
    fieldsForm: {
        name: '',
        email: '',
        text: '',
        select: '',
        number: [],
        check: false,
        data: ''
    }
}


const newForm = new getDataFromInputs (data)

const btn = document.querySelector('.btn')

btn.addEventListener('click', () => {
    console.log('newForm.getList', newForm.getListForm)
    console.log('newForm.clearAllFields()', newForm.clearAllFields())
})
