import { LocalStorage } from '../localStorage/localStorage'
import axios from 'axios'

export class Token extends LocalStorage {

    constructor(key, value) {
        super(key, value)
    }
    tokenHeaders() {
        const tokenHeader = super.getLocalStorage
        console.log('tokenHeaders', tokenHeader)
        return  axios.defaults.headers.common['Authorization'] = `Bearer ${tokenHeader}`
    }
}

