console.warn('LocalStorage подключен')

class LocalStorage {

    constructor(key, value) {
        this.key = key
        this.value = value
    }
    save() {
        return localStorage.setItem(`${this.key}`, JSON.stringify(this.value))
    }
    delete() {
        localStorage.removeItem(`${this.key}`)
    }
    clear() {
        localStorage.clear()
    }
    get getLocalStorage() {
        return JSON.parse(localStorage.getItem(`${this.key}`)) || null
    }
}

// Удалить потом

class FindUser extends LocalStorage {

    constructor(key, value) {
        super(key, value)
    }

    // Первый способо поиска
    findUser(payload) {
        // Получаем всех юзеров
        const getAllUsers = super.getLocalStorage
        // Проверяем getAllUsers является ли массивом
        if (Array.isArray(getAllUsers) && getAllUsers.length > 0) {
            const result = getAllUsers.find(item => Object.values(item).includes(payload));
            return result === undefined ? 'Такой пользователь не найден' : result
        } else {
            return Object.values(getAllUsers).find(item => item === payload)
           }
        }

    // Поиск с выводомо несколькими результатами

    filterFindUser(payload) {

        let filterSearch = []
        // Получаем всех юзеров
        const getAllUserList = super.getLocalStorage
        const { field, value } = payload
        if (field === 'password') {
            getAllUserList.map(item => {
                if (item[field] === value) {
                    filterSearch = [...filterSearch, item]
                }
            })
        } else {
            getAllUserList.map(item => {
                if (item[field].toLowerCase() === value.toLowerCase()) {
                    filterSearch = [...filterSearch, item]
                }
            })
        }
        return filterSearch.length > 0 ? filterSearch : 'Такие пользователи не найдены'
    }
}

// const user = {id: 1, name: 'Петя', password: 123456, lastName: 'Петров'}
// const users = [
//     {id: 1, name: 'Петя', password: 123456, lastName: 'Петров'},
//     {id: 1, name: 'Илья', password: 777777, lastName: 'Илья'},
//     {id: 1, name: 'Илья', password: 888888, lastName: 'Петров'},
//     {id: 1, name: 'Вася', password: 999999, lastName: 'Васильев'}
// ]

