console.log('Подключен')

class getDataFromInputs {

    constructor(payload) {

        const { form, tagName, fieldsForm, validateInputs, disabledBtn } = payload

        this.form = form || ''
        this.tagName = tagName || ''
        this.fieldsForm = fieldsForm || {}
        this.validateInputs = validateInputs || {}
        this.button = disabledBtn
        this.setInput = []
        this.errors = []
        this.selectTag = ''
        this.inputText = ''
        this.selectinput = ''
        this.selectBtn = null
        this.count = false
        this.text = ''
        this.classCheckBox = ''
        this.classTextError = ''
        this.minLenght = ''
        this.maxLenght = ''
        this.getInputs()
    }

    // Получаем все input и textarea относящиеся к форме которую мы передаем в классе. Записываем все общий массив setInput для переиспользования глобально

    getInputs() {
        const getInputs = document.querySelectorAll(`.${this.form} > input`)
        const getTextarea = document.querySelectorAll(`.${this.form} > textarea`)
        if (this.button) {
            this.selectBtn = document.querySelector(`.${this.form} > button`)
            this.selectBtn.setAttribute('disabled', 'disabled')
        }
        this.setInput = [...this.setInput, ...getTextarea, ...getInputs]
        this.createFieldError()
        this.listenerInputs()
    }

    // Создаем новый массив для записи ошибок относящиеся к полям

    createFieldError () {
        for (const key in this.fieldsForm) {
            this.errors = [...this.errors, { name: key, error: '' }]
        }
    }

    // Слушаем все input относящиеся к форме и чистим

    listenerInputs() {
        this.setInput.forEach((input) => {
            input.addEventListener('input', (e) => this.setInputsForms(e))
            input.removeEventListener('input', (e) => this.setInputsForms(e))
        })
    }

    // Записываем в форму все получаемые данные c input

    setInputsForms (e) {
        for (const key in this.fieldsForm) {
            const getAttribute = e.target.getAttribute(`${this.tagName}`)
            // Если attribute input совпадает с ключем передаваемой формы, то мы записываем данные в форму
            if (getAttribute === key) {
                // Записываем глобально данные активного input
                this.selectTag = getAttribute
                this.selectinput = e.target
                this.inputText = e.target.value
                // Если тип input равен checkbox, то мы запускаем другую запись
                if (e.target.type === 'checkbox') {
                    // Если тип поля формы равен boolean , то мы перезаписываем данные этой формы на true или false
                    if (typeof this.fieldsForm[this.selectTag] === 'boolean') {
                        this.fieldsForm[this.selectTag] = this.fieldsForm[key] ? false : true
                        this.checkedInputRequired()
                    } else {
                        // Если в форме поля массива присуствует значения поступающее с input, то мы удаляем это значение
                        if (this.fieldsForm[this.selectTag].includes(this.inputText)) {
                            this.fieldsForm[this.selectTag] = this.fieldsForm[this.selectTag].filter( function(payload) { return payload != e.target.value } )
                        // Иначе записываем в массив
                            this.checkedInputRequired()
                        } else {
                            this.fieldsForm[this.selectTag] = [...this.fieldsForm[this.selectTag], this.inputText]
                            this.checkedInputRequired()

                        }
                    }
                //    Если данные input поступает строкой, то мы записываем в обычном виде
                } else {
                    this.fieldsForm[this.selectTag] = this.inputText
                    //    Запускаем валидацию на длину input
                    this.validateLengthInput()
                    //    Запускаем валидацию на тип input
                    this.validateTypeInput()
                    // this.checkedInputRequired()
                }
            }
        }
    }

    // Проверка input на заданную длину

    validateLengthInput() {
        const { minLength, maxLength, text, classTextError, classCheckbox } = this.validateInputs[this.selectTag]
        this.addParamsValidateInput({ minLength, maxLength, text, classTextError, classCheckbox })
        if (this.inputText?.trim()?.length <= minLength) {
            this.mainAddErrorLayout()
        } else if (this.inputText?.trim()?.length > maxLength) {
            // Если валидацию не проходим, то записываем в массив ошибок ошибку относяшиеся к этому ключу поля, передав значение текста
            this.mainAddErrorLayout()
        } else {
            // Если проходим проверку, то удаляем с DOM дерева созданную ошибку
            this.mainRemoveErrorLayout()
        }
    }

    // Проверка input на тип поля

    validateTypeInput() {
        const { type, text, classTextError, classCheckbox } = this.validateInputs[this.selectTag]
        this.addParamsValidateInput({ type, text, classTextError, classCheckbox })
        if (type === 'email') {
            if (!this.validEmail(this.form.email)) {
                this.mainAddErrorLayout()
            } else {
                this.mainRemoveErrorLayout()
            }
        }
    }


    checkedInputRequired() {
        // Разобратся !
        const { required, text, classTextError, classCheckbox } = this.validateInputs[this.selectTag]
        this.addParamsValidateInput({ text, classTextError, classCheckbox })
        if (required) {
            if (typeof this.fieldsForm[this.selectTag] === 'boolean') {
                console.log('this.fieldsForm[this.selectTag]', this.fieldsForm[this.selectTag])
                if (!this.fieldsForm[this.selectTag]) {
                    this.mainAddErrorLayout()
                } else if (this.fieldsForm[this.selectTag]) {
                    this.mainRemoveErrorLayout()
                }
            } else if (Array.isArray(this.fieldsForm[this.selectTag]) && typeof this.fieldsForm[this.selectTag] === 'object') {
                if (this.fieldsForm[this.selectTag].length === 0) {
                    this.mainAddErrorLayout()
                } else {
                    this.mainRemoveErrorLayout()
                }
            }
        } else {
            return
        }
    }

    // Проверка input на email

    validEmail () {
        let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(this.inputText);
    }


    // Общий класс для добавления ошибок

    mainAddErrorLayout () {
        // Если валидацию не проходим, то записываем в массив ошибок ошибку относяшиеся к этому ключу поля
        // this.addErrorField()
        // Выводим ошибку под input
        this.addErrorLayout()
        this.button ? this.observeErrorLayout() : ''
    }

    // Общий классс для удаление ошибки

    mainRemoveErrorLayout () {
        this.removeErrorLayout()
        this.button ? this.observeErrorLayout() : ''
    }

    // Удаляем класс с error

    removeErrorLayout() {
        const errorLayout = document.querySelector(`.${this.classCheckBox} > .inputError`)
        if (errorLayout !== null) {
            errorLayout.remove()
        } else if (this.selectinput.nextSibling?.classList?.contains(`${this.classTextError}`)) {
            this.selectinput.nextSibling.remove()
            this.errors.forEach((field) => {
                if (field.name === this.selectTag) {
                    field.error = ''
                }
            })
        }
    }

    // Добавляем ошибку в массив ошибок

    // addErrorField(text) {
    //     this.errors.forEach((field) => {
    //         if (field.name === this.selectTag) {
    //             field.error = text
    //         }
    //     })
    // }

    // Выводим ошибку под input

    addErrorLayout () {
        const attribute = this.selectinput.getAttribute(`${this.tagName}`)
        if (this.selectinput?.nextSibling?.hasChildNodes()) {
            return
        } else {
            this.errors.forEach((input) => {
                const { name } = input
                const checkBoxLabel = document.querySelector(`.${this.classCheckBox}`)
                if (attribute === name) {
                   if (this.selectinput.type === 'checkbox') {
                       checkBoxLabel.insertAdjacentHTML('beforeend',
                           `<span style="display: block"  class="inputError ${this.classTextError}">${this.text}</span>`)
                    } else {
                        checkBoxLabel.insertAdjacentHTML('afterend',
                            `<span style="display: block" class="inputError ${this.classTextError}">${this.text}</span>`)
                    }
                }
            })
        }
    }

    // Добавляем глобально параметры validate

    addParamsValidateInput (payload) {
        const { minLength, maxLength, text, classTextError,classCheckbox } = payload

        this.minLenght = minLength
        this.maxLenght = maxLength
        this.text = text
        this.classTextError = classTextError
        this.classCheckBox = classCheckbox
    }

    // TODO слушатель на ошибку

    observeErrorLayout () {
        const checkError = document.querySelectorAll('.inputError')
        if (checkError.length > 0) {
            this.selectBtn.setAttribute('disabled', 'disabled')
            return false
        } else {
            this.selectBtn.removeAttribute('disabled')
            return true
        }
    }

    // Получаем данные с заполненной формы

    get getListForm () {
        return this.fieldsForm
    }
}

const data = {
    form: 'form-req',
    tagName: 'name',
    disabledBtn: true,
    validateInputs: {
        name: {
            minLength: 0,
            classTextError: 'name-error',
            classCheckbox: 'name',
            text: '*Поля имя не должен быть пустым :)'
        },
        email: {
            type: 'email',
            classCheckbox: 'email',
            classTextError: 'email-error',
            text: '*Требуется корректный e-mail :('
        },
        password: {
            minLength: 6,
            maxLength: 16,
            classCheckbox: 'password',
            classTextError: 'password-error',
            text: '*Пароль должен быть от 6 до 16 символов!'
        },
        about: {
            minLength: 10,
            maxLength: 120,
            classTextError: 'about-error',
            classCheckbox: 'about',
            text: '*Поле about должен быть от 10 до 120 символов'
        },
        buy: {
            required: true,
            classTextError: 'about-error',
            classCheckbox: 'checkbox-group',
            text: '*Выберите одно из значений'
        },
        politic: {
            required: true,
            classTextError: 'about-error',
            classCheckbox: 'checkbox',
            text: '*Обязательное для заполнения'
        }
    },
    fieldsForm: {
        politic: false,
        buy: [],
        about: '',
        password: '',
        email: '',
        name: ''
    }
}

let count = 0

const addLayoutData = () => {
    if (count === 1) {
        const getDataLayoutForm = document.querySelector('.form-data')
        getDataLayoutForm.remove()
        count = 0
        addLayoutData()
    } else {
        const getDataLayoutForm = document.createElement('div')
        const resultWrapper = document.querySelector('.form-result')
        getDataLayoutForm.className = 'form-data'
        for (const key in newForm.getListForm) {
            const layout = `<p>Поле(${key}): <span>${newForm.getListForm[key]}</span></p> `
            getDataLayoutForm.insertAdjacentHTML('afterbegin', layout)
        }
        resultWrapper.append(getDataLayoutForm)
        count = count + 1
    }
}

const dataForm = document.querySelector('.form-result')

let mutationObserver = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
        if (mutation.target.classList.contains('form-result')) {
            const noData = document.querySelector('.no-data')
            noData?.remove()
        }
    });
});

mutationObserver.observe(dataForm, {
    attributes: true,
    characterData: true,
    childList: true,
});

const newForm = new getDataFromInputs (data)

const btn = document.querySelector('.btn-send')

btn?.addEventListener('click', () => {
    const check = newForm.observeErrorLayout()
    if (check) {
        console.log('newForm.getList', newForm.getListForm)
        addLayoutData()
    } else {
        console.log('Нет')
    }
    // console.log('newForm.getList', newForm.getListForm)
})
